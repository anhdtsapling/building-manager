import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, TouchableOpacity, RefreshControl, Image } from 'react-native';
import { Container, Content, Card, CardItem, Body, Text, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Loading from './Loading';
import Error from './Error';
import Header from './Header';
import Spacer from './Spacer';

const RecipeListing = ({
  error,
  loading,
  recipes,
  reFetch,
}) => {
  recipes = [
    {
      "id": 1,
      "slug": "this-is-an-article",
      "title": "Thư điện tử",
      "body": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "John Smith",
      "category": 1,
      "image": "http://207.148.67.121:1338/pictures/thu-dien-tu.png",
      "ingredients": [
        "sed do eiusmod tempor incididunt",
        "aute irure dolor in",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    },
    {
      "id": 2,
      "slug": "dummy-text-of-the-printing",
      "title": "Tin tức",
      "body": "Typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "Jane Doe",
      "category": 2,
      "image": "http://207.148.67.121:1338/pictures/news.png",
      "ingredients": [
        "sed do eiusmod tempor incididunt",
        "aute irure dolor in",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    },
    {
      "id": 3,
      "slug": "survived-not-only-five",
      "title": "Danh bạ",
      "body": "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "Jane Doe",
      "category": 1,
      "image": "http://207.148.67.121:1338/pictures/danh-ba.png",
      "ingredients": [
        "sed do eiusmod tempor incididunt",
        "aute irure dolor in",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    },
    {
      "id": 4,
      "slug": "standard-dummy-text-ever",
      "title": "Quản lí công",
      "body": "Has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "John Smith",
      "category": 2,
      "image": "http://207.148.67.121:1338/pictures/quan-li-cong.png",
      "ingredients": [
        "sed do eiusmod tempor incididunt",
        "aute irure dolor in",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    },
    {
      "id": 5,
      "slug": "remaining-essentially-unchanged",
      "title": "Văn bản",
      "body": "Industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "John Smith",
      "category": 2,
      "image": "http://207.148.67.121:1338/pictures/van-ban.png",
      "ingredients": [
        "sed do eiusmod tempor incididunt",
        "aute irure dolor in",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    },
    {
      "id": 6,
      "slug": "only-five-centuries",
      "title": "Khảo sát trực tuyyến ",
      "body": "Standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "John Smith",
      "category": 2,
      "image": "http://207.148.67.121:1338/pictures/khao-sat-truc-tuyen.png",
      "ingredients": [
        "sed do eiusmod tempor incididunt",
        "aute irure dolor in",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    },
    {
      "id": 7,
      "slug": "but-also-the-leap-into",
      "title": "Quản lí tờ trình",
      "body": "Dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "Jane Doe",
      "category": 1,
      "image": "http://207.148.67.121:1338/pictures/quan-li-to-trinh.png",
      "ingredients": [
        "aute irure dolor in",
        "sed do eiusmod tempor incididunt",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    },
    {
      "id": 8,
      "slug": "electronic-typesetting",
      "title": "Đặt xe",
      "body": "Text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "John Smith",
      "category": 2,
      "image": "http://207.148.67.121:1338/pictures/car.png",
      "ingredients": [
        "sed do eiusmod tempor incididunt",
        "aute irure dolor in",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    },
    {
      "id": 9,
      "slug": "essentially-unchanged",
      "title": "Đặt phòng họp",
      "body": "Industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "John Smith",
      "category": 2,
      "image": "http://207.148.67.121:1338/pictures/dat-phong-hop.png",
      "ingredients": [
        "sed do eiusmod tempor incididunt",
        "aute irure dolor in",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    },
    {
      "id": 10,
      "slug": "standard-text",
      "title": "Hỗ trợ",
      "body": "Unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      "author": "Jane Doe",
      "category": 2,
      "image": "http://207.148.67.121:1338/pictures/ho-tro.png",
      "ingredients": [
        "sed do eiusmod tempor incididunt",
        "aute irure dolor in",
        "do eiusmod tempor",
        "uis aute irure dolor in",
        "doloremque laudantium",
        "cupidatat non proident"
      ],
      "method": [
        "iste natus error sit voluptatem accusantium doloremque laudantium",
        "magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet",
        "sed quia non numquam eius modi tempora incidunt ut labore"
      ]
    }
  ]
  // Loading
  if (loading) return <Loading />;

  // Error
  if (error) return <Error content={error} />;

  const keyExtractor = item => item.id;

  const onPress = item => Actions.recipe({ match: { params: { id: String(item.id) } } });

  return (
    <Container>
      <Content padder>
        <FlatList
          numColumns={2}
          data={recipes}
          renderItem={({ item }) => (
            <Card transparent style={{ paddingHorizontal: 6 }}>
              <CardItem cardBody>
                <TouchableOpacity onPress={() => onPress(item)} style={{ flex: 1 }}>
                  <Image
                    source={{ uri: item.image }}
                    style={{
                      height: 80,
                      resizeMode: "contain",
                      flex: 1,
                      borderRadius: 5,
                    }}
                  />
                </TouchableOpacity>
              </CardItem>
              <CardItem cardBody>
                <Body>
                  <Spacer size={10} />
                  <Text style={{ fontWeight: '800', textAlign: 'center', width: '100%' }}>{item.title}</Text>
                  <Spacer size={15} />
                  <Spacer size={5} />
                </Body>
              </CardItem>
            </Card>
          )}
          keyExtractor={keyExtractor}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={reFetch}
            />
          }
        />

        <Spacer size={20} />
      </Content>
    </Container>
  );
};

RecipeListing.propTypes = {
  error: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  recipes: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  reFetch: PropTypes.func,
};

RecipeListing.defaultProps = {
  error: null,
  reFetch: null,
};

export default RecipeListing;
